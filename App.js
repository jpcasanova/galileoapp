import React, {useDebugValue} from 'react';
import Providers from './app/navigation';

const App = () => {
  return <Providers />;
};

export default App;
