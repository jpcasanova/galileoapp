import React, {useContext} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import FormButton from '../components/FormButton';
import {AuthContext} from '../navigation/AuthProvider';
import {SafeAreaView} from 'react-native-safe-area-context';
export default function HomeScreen() {
  const {user, logout} = useContext(AuthContext);
  return (
    <View style={styles.container}>
      <Text>HomeScreen {user.uid}</Text>
      <Text>HomeScreen {user.uid}</Text>
      <Text>HomeScreen {user.uid}</Text>
      <Text>HomeScreen {user.uid}</Text>
      <FormButton buttonTitle="Logout" onPress={() => logout()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    color: '#000',
  },
  imageProfile: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  containerProfile: {
    flexDirection: 'row',
    margin: 20,
  },
  name: {
    flexDirection: 'row',
    // margin: 20,
  },
});
