import React, {useContext} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import FormButton from '../components/FormButton';
import {AuthContext} from '../navigation/AuthProvider';

export default function ProfileScreen() {
  const {user, logout} = useContext(AuthContext);
  return (
    <View style={styles.container}>
      <View style={styles.containerProfile}>
        <Image
          source={{uri: 'https://randomuser.me/api/portraits/women/75.jpg'}}
          style={styles.imageProfile}
        />
        <Text style={styles.name}>Mandy Moore</Text>
        <Text style={styles.name}>Mandy Moore</Text>
      </View>
      <Text>HomeScreen {user.uid}</Text>
      <FormButton buttonTitle="Logout" onPress={() => logout()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  imageProfile: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  containerProfile: {
    flexDirection: 'row',
    margin: 20,
  },
  name: {
    flexDirection: 'row',
    // margin: 20,
  },
});
