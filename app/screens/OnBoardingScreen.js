import React from 'react';
import {View, Text, Image, StyleSheet, Button} from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';

const Skip = ({...props}) => <Button title="Omitir" color="#fff" {...props} />;
const Next = ({...props}) => (
  <Button title="Siguiente" color="#fff" {...props} />
);
const Done = ({...props}) => <Button title="Listo!" color="#fff" {...props} />;

const OnBoardingScreen = ({navigation}) => {
  return (
    <Onboarding
      SkipButtonComponent={Skip}
      NextButtonComponent={Next}
      DoneButtonComponent={Done}
      onSkip={() => navigation.replace('Login')}
      onDone={() => navigation.replace('Login')}
      pages={[
        {
          backgroundColor: '#66c0c0',
          image: <Image source={require('../../assets/img1.png')} />,
          title: 'Onboarding1',
          subtitle: 'Done with React Native Onboarding Swiper',
        },
        {
          backgroundColor: '#8873c0',
          image: <Image source={require('../../assets/img2.png')} />,
          title: 'Onboarding2',
          subtitle: 'Done with React Native Onboarding Swiper',
        },
        {
          backgroundColor: '#c19912',
          image: <Image source={require('../../assets/img3.png')} />,
          title: 'Onboarding3',
          subtitle: 'Done with React Native Onboarding Swiper',
        },
      ]}
    />
  );
};

export default OnBoardingScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
